import {
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
import {NutrientFact, Product as ProductInterface, Recipe, RecipeLineItem, UnitOfMeasure} from "./supporting-files/models";
import {
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits,
    SumUnitsOfMeasure
} from "./supporting-files/helpers";
import {RunTest, ExpectedRecipeSummary} from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

class Product {
    constructor(
        public productName: string,
        public brandName: string,
        public nutrientFacts: NutrientFact[]
    ) {}
}

class Supplier {
    public costPerBaseUnit: number;

    constructor(
        public supplierName: string,
        public supplierProductName: string,
        public supplierPrice: number,
        public supplierProductUoM: UnitOfMeasure
    ) {
        this.costPerBaseUnit = GetCostPerBaseUnit({
            supplierName: this.supplierName,
            supplierProductName: this.supplierProductName,
            supplierPrice: this.supplierPrice,
            supplierProductUoM: this.supplierProductUoM
        })
    }

    getCostPerBaseUnit = () => this.costPerBaseUnit;
}

class ProductSupplier {
    constructor(
        public product: Product,
        public supplier: Supplier
    ) {}
}

class IngredientSupplier {
    constructor(
        public ingredient: RecipeLineItem,
        public supplier: ProductSupplier
    ) {}

    getSupplier() {
        return this.supplier.supplier;
    }

    getProduct() {
        return this.supplier.product;
    }

    getQuantity() {
        return this.ingredient.unitOfMeasure.uomAmount;
    }
}

const getProductSuppliers = (product: ProductInterface) => {
    return product.supplierProducts.map((supplier) => {
        const _product = new Product(product.productName, product.brandName, product.nutrientFacts);
        const _supplier = new Supplier(supplier.supplierName, supplier.supplierProductName, supplier.supplierPrice, supplier.supplierProductUoM)

        return new ProductSupplier(_product, _supplier);
    })
}

const getCheapestProductSupplier = (suppliers: ProductSupplier[]) => {
    suppliers.sort((productSupplierA, productSupplierB) => {
        const priceA = productSupplierA.supplier.getCostPerBaseUnit()
        const priceB = productSupplierB.supplier.getCostPerBaseUnit()

        if (priceA > priceB) return 1;
        if (priceA < priceB) return -1;
        return 0;
    })

    return suppliers[0]
}

const getIngredientSupplier = (lineItem: RecipeLineItem) => {
    return GetProductsForIngredient(lineItem.ingredient).reduce((o, product) => {
        return [...o, ...getProductSuppliers(product)]
    }, [])
}

const getNutrientFacts = (products: Product[]) => {
    // get all nutrient facts
    const nutrientFacts = products.reduce((o, product) => {
        const nutrientFacts = product.nutrientFacts.map((n) => GetNutrientFactInBaseUnits(n))
        return [...o, ...nutrientFacts]
    }, [])

    // sort nutrient facts
    nutrientFacts.sort((a, b) => a.nutrientName.localeCompare(b.nutrientName))

    // group nutrient facts
    return nutrientFacts.reduce((o, n) => {
        if (!(n.nutrientName in o)) {
            o[n.nutrientName] = n
            return o;
        }

        o[n.nutrientName].quantityAmount = SumUnitsOfMeasure(o[n.nutrientName].quantityAmount, n.quantityAmount)
        return o;
    }, {})
}

const calculateIngredientCost = (ingredientSuppliers: IngredientSupplier[]) => {
    return ingredientSuppliers.reduce((o, i) => {
        o += i.getSupplier().getCostPerBaseUnit() * i.getQuantity()
        return o;
    }, 0)
}

class RecipeSummaryFactory {
    public static generate(recipe: Recipe) {
        const ingredientSuppliers = recipe.lineItems.map((item) => {
            const productSupplier = getCheapestProductSupplier(getIngredientSupplier(item))
            return new IngredientSupplier(item, productSupplier)
        })

        const products = ingredientSuppliers.map((i) => i.getProduct())
        const nutrientsAtCheapestCost = getNutrientFacts(products)
        const cheapestCost = calculateIngredientCost(ingredientSuppliers);

        return { cheapestCost, nutrientsAtCheapestCost }
    }
}

recipeData.forEach((recipe) => {
    recipeSummary[recipe.recipeName] = RecipeSummaryFactory.generate(recipe)
})

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
